import hashlib
import itertools
import string

rainbow = {}
characters = list(string.printable)
permutations = itertools.permutations(characters, 8)
for permutation in permutations:
    orig = ''.join(permutation).encode('utf-8')
    m = hashlib.md5()
    m.update(orig)
    rainbow[m.hexdigest()] = orig

# print(rainbow['5be360b589a57a2a7c2fe6fb8fe84408'])